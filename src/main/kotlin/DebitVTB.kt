class DebitVTB: DebitCard() {
    val cashBackK = 0.05 // кэшбэк 5% при оплате покупки дороже 5000.
    override var balance: Int = 0

    //balance += 200 // почему нельзя так написать? Сеттер нужен? Не понимаю. Сеттер и геттер же для работы с инстансом класса.

    /*override fun pay(amount: Int): Boolean {
        if (amount >= 5000){
            balance += amount * cashBackK
        }

        return super.pay(amount)
    }*/

    override fun pay(amount: Int): Boolean {
        super.pay(amount)
        if (amount >= 5000){
            balance += amount * cashBackK.toInt()
        }
        return true
    }
}