abstract class DebitCard: BankCard() {
    override var balance: Int = 0

    override fun replenish(amount: Int) {
        balance += amount
        println("Счёт пополнен на $amount")
    }

    override fun pay(amount: Int): Boolean {
        return if (balance >= amount) {
            balance -= amount
            println("Оплата на $amount проведена успешно")
            true
        } else {
            println("Недостаточно средств")
            false
        }
    }

    override fun getBalanceInfo() {
        println("Баланс средств - $balance")
    }

    override fun getFullInfo() {
        println("Баланс средств - $balance")
    }
}