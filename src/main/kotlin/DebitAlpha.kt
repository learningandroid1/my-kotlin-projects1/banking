class DebitAlpha: DebitCard() {
    // бонусные балы в виде 1% от покупок.
    private var bonusBalance: Double = 0.0

    override fun pay(amount: Int): Boolean {
        bonusBalance += amount * bonusK
        return super.pay(amount)
    }

    override fun getFullInfo() {
        super.getFullInfo()
        println("На Вашем счёте $bonusBalance бонусов")
    }

    companion object {
        const val bonusK = 0.01
    }
}