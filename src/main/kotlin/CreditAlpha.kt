class CreditAlpha(creditLimit: Int): CreditCard(creditLimit) {
    // пополнение накопительного счёта на 0,5% от вносимых средств.
    private var savings = 0

    override fun replenish(amount: Int) {
        super.replenish(amount)
        savings += amount/savingsK
        println("Накопительный счёт пополнен на ${amount/savingsK}")
    }

    override fun getFullInfo() {
        super.getFullInfo()
        println("Баланс накопительного счёта: $savings")
    }

    companion object {
        const val savingsK = 200 // Имитация 0,5%
    }
}