abstract class BankCard {
    protected abstract var balance: Int

    abstract fun replenish(amount: Int)
    abstract fun pay(amount: Int): Boolean
    abstract fun getBalanceInfo()
    abstract fun getFullInfo()
}