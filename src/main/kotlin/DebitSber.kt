class DebitSber: DebitCard() {
    // кэшбэк 5% при оплате покупки дороже 5000.
    override var balance: Int = 0

    override fun pay(amount: Int): Boolean {
        super.pay(amount)
        if (amount >= 5000){
            balance += amount/cashBackK
        }
        return true
    }

    companion object {
       const val cashBackK = 20
    }
}