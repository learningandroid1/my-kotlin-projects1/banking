class CreditVTB(creditLimit: Int): CreditCard(creditLimit) {
    // кэшбэк 5% при любых покупках, при тратах по карте более 30000.
    private var expenses = 0
    override var balance: Int = 0

    override fun pay(amount: Int): Boolean {
        super.pay(amount)
        if (expenses >= cashBackTreshold) {
            ownFunds += amount/cashBackK
        }
        expenses += amount
        return true
    }

    override fun getFullInfo() {
        super.getFullInfo()
        println("Расходы: $expenses")
        if (expenses >= cashBackTreshold) println("Статус кэшбэка: активен")
        else println("Статус кэшбэка: не активен")
    }

    companion object{
        const val cashBackTreshold = 30000
        const val cashBackK = 20
    }
}

