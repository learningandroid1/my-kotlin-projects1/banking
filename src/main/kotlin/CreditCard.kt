abstract class CreditCard(private var creditLimit: Int): BankCard() {
    override var balance: Int = 0

    private var creditFunds = creditLimit
    var ownFunds = 0


    override fun replenish(amount: Int) {
        if (creditFunds == creditLimit) {
            ownFunds += amount
            println("Счёт пополнен на $amount")
        } else {
            val someFunds: Int = creditLimit - creditFunds
            creditFunds += someFunds
            ownFunds += amount - someFunds
            println("Счёт пополнен на $amount")
        }
    }

    override fun pay(amount: Int): Boolean {
        if ((ownFunds + creditFunds) >= amount) {
            if (ownFunds >= amount) {
                ownFunds -= amount
            } else {
                val someFunds = ownFunds - amount
                ownFunds = 0
                creditFunds += someFunds
            }
            println("Оплата на $amount проведена успешно")
            return true
        } else {
            println("Недостаточно средств")
            return false}
    }

    override fun getBalanceInfo() {
        println("Баланс средств - ${ownFunds + creditFunds} ")
    }

    override fun getFullInfo() {
        println("Кредитный лимит: $creditLimit")
        println("Кредитные средства: $creditFunds")
        println("Собственные средства: $ownFunds")
    }
}